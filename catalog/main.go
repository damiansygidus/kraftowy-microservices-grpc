package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	pb "kraftowy-microservices-grpc/catalog/ecommerce"
)

func getTaxConnection(host string) (*grpc.ClientConn, error) {
	wd, _ := os.Getwd()
	parentDir := filepath.Dir(wd)
	certFile := filepath.Join(parentDir, "keys", "cert.pem")
	creds, _ := credentials.NewClientTLSFromFile(certFile, "")

	return grpc.Dial(host, grpc.WithTransportCredentials(creds))
}

func findCountryByCode(code string) (pb.Country, error) {
	c1 := pb.Country{Code: "PL", Name: "Poland", VatRate: 0.23}
	c2 := pb.Country{Code: "DE", Name: "Germany", VatRate: 0.19}
	c3 := pb.Country{Code: "UK", Name: "United Kingdom", VatRate: 0.2}

	countries := map[string]pb.Country{
		c1.Code: c1,
		c2.Code: c2,
		c3.Code: c3,
	}

	found, ok := countries[code]
	if ok {
		return found, nil
	}

	return found, errors.New("Country not found")
}

func getFakeProducts() []*pb.Product {
	p1 := pb.Product{Id: 1, Name: "macbook-pro-15", Description: "Intel i9 2.6 GHz, 32 GB RAM, 512 GB", NetPriceInCents: 200112}
	p2 := pb.Product{Id: 2, Name: "dell-p2416d", Description: "24 inch, resolution 2560x1140, HDR", NetPriceInCents: 25442}
	p3 := pb.Product{Id: 3, Name: "trackpad-2017", Description: "Bluetooth 5.0 connectivity", NetPriceInCents: 18602}

	return []*pb.Product{&p1, &p2, &p3}
}

func getProductsWithTaxApplied(country pb.Country, products []*pb.Product) []*pb.Product {
	host := "localhost:11443"

	conn, err := getTaxConnection(host)
	if err != nil {
		log.Fatalf("Did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewTaxClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	getProductsWithTaxApplied := make([]*pb.Product, 0)

	for _, product := range products {
		r, err := c.ApplyTax(ctx, &pb.TaxRequest{Country: &country, Product: product})
		if err == nil {
			getProductsWithTaxApplied = append(getProductsWithTaxApplied, r.GetProduct())
		} else {
			log.Println("Failed to apply tax.", err)
		}
	}

	if len(getProductsWithTaxApplied) > 0 {
		return getProductsWithTaxApplied
	}
	return products
}

func handleGetProducts(w http.ResponseWriter, req *http.Request) {
	products := getFakeProducts()
	w.Header().Set("Content-Type", "application/json")

	q := req.URL.Query()
	code := q.Get("country")

	if code == "" {
		json.NewEncoder(w).Encode(products)
		return
	}

	country, err := findCountryByCode(code)
	if err != nil {
		json.NewEncoder(w).Encode(products)
		return
	}

	getProductsWithTaxApplied := getProductsWithTaxApplied(country, products)
	json.NewEncoder(w).Encode(getProductsWithTaxApplied)
}

func handleMainPage(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "It's working!")
}

func main() {
	port := "11080"

	http.HandleFunc("/", handleMainPage)
	http.HandleFunc("/products", handleGetProducts)

	fmt.Println("Server is running on", port)
	http.ListenAndServe(":"+port, nil)
}

package main

import (
	"github.com/stretchr/testify/suite"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type CreateElementsTestSuite struct {
	suite.Suite
}

type HandleRequestsTestSuite struct {
	suite.Suite
}

func (suite *CreateElementsTestSuite) TestGetFakeProducts() {
	//when
	p := getFakeProducts()

	//then
	suite.NotEmpty(p)
	suite.Equal(p[0].Name, "macbook-pro-15")
	suite.Equal(p[1].Description, "24 inch, resolution 2560x1140, HDR")
	suite.Equal(p[2].NetPriceInCents, int32(18602))
}

func (suite *CreateElementsTestSuite) TestFindCountryByCode() {
	//given
	c1 := "UK"
	c2 := "NIL"

	//when
	country1, err1 := findCountryByCode(c1)
	country2, err2 := findCountryByCode(c2)

	//then
	suite.Equal(country1.Code, c1)
	suite.Nil(err1)

	suite.Empty(country2.Code)
	suite.NotNil(err2)
}

func (suite *HandleRequestsTestSuite) TestHandleGetProducts() {
	//given
	expected := "[{\"id\":1,\"name\":\"macbook-pro-15\",\"description\":\"Intel i9 2.6 GHz, 32 GB RAM, 512 GB\",\"net_price_in_cents\":200112},"
	handler := http.HandlerFunc(handleGetProducts)
	recorder := httptest.NewRecorder()

	//when
	req, _ := http.NewRequest("GET", "", nil)
	handler(recorder, req)

	//then
	response, _ := ioutil.ReadAll(recorder.Body)
	suite.Contains(string(response), expected)
}

func (suite *HandleRequestsTestSuite) TestHandleMainPage() {
	//given
	expected := "It's working!"
	handler := http.HandlerFunc(handleMainPage)
	recorder := httptest.NewRecorder()

	//when
	req, _ := http.NewRequest("GET", "", nil)
	handler(recorder, req)

	//then
	response, _ := ioutil.ReadAll(recorder.Body)
	suite.Contains(string(response), expected)
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(CreateElementsTestSuite))
	suite.Run(t, new(HandleRequestsTestSuite))
}
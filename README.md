# kraftowy-microservices-grpc
## Overview
POC project of two microservices with gRPC communication based on a e-commerce platform:

1. __Catalog__ - written in Go, exposes a REST API that returns a list of products, can consume one URL parameter _country_ with country code value (for example _PL_)
2. __Tax__ - written in Python, returns recived product with _Gross_ object that contains country tax details and product price with tax applied

## Getting started
1. Generate your keys in `keys` folder
```bash
mkdir keys
cd keys
openssl req -x509 -newkey rsa:4096 -keyout private.key -out cert.pem -days 365 -nodes -subj '/CN=localhost'
```
2. (optional) If you want to genarate _protoc_ files by yourself install __protobuf__:
```bash
git clone https://github.com/protocolbuffers/protobuf.git
cd protobuf
./configure
make
sudo make install
sudo ldconfig
```
_protoc_ plugin for Go:
```bash
go get -u google.golang.org/grpc
go get -u github.com/golang/protobuf/proto
go get -u github.com/golang/protobuf/protoc-gen-go
sudo ln -snf $GOROOT/bin/protoc-gen-go /usr/local/bin
```
and for Python as well:
```bash
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install grpcio grpcio-tools
```
3. (optional) You can then generate your protoc files for _tax_ app:
```bash
cd ./tax
python -m grpc_tools.protoc -I=.. --python_out=. --grpc_python_out=. ../ecommerce.proto
```
and for _catalog_ app:
```bash
cd ./catalog
mkdir ecommerce
protoc -I=.. --go_out=plugins=grpc:ecommerce ../ecommerce.proto
```
4. Run your microservices:
```bash
cd ./tax
source venv/bin/activate
python server.py 11443
```
and open new tab to run `catalog` app:
```bash
cd ~/microservices-grpc-go-python/catalog
go run main.go
```

## Local testing
with cURL:
```bash
curl http://localhost:11080/products\?country\=PL
```
or simply in any browser:
```bash
http://localhost:11080/products?country=PL
```

## Example results
for request to local _Catalog_ app with port `11080`:
```
localhost:11080/products?country=UK
```
we get:
```json
[
   {
      "id": 1,
      "name": "macbook-pro-15",
      "description": "Intel i9 2.6 GHz, 32 GB RAM, 512 GB",
      "net_price_in_cents": 200112,
      "gross": {
         "country": {
            "code": "UK",
            "name": "United Kingdom",
            "vat_rate": 0.2
         },
         "price_in_cents": 240134
      }
   },
   {
      "id": 2,
      "name": "dell-p2416d",
      "description": "24 inch, resolution 2560x1140, HDR",
      "net_price_in_cents": 25442,
      "gross": {
         "country": {
            "code": "UK",
            "name": "United Kingdom",
            "vat_rate": 0.2
         },
         "price_in_cents": 30530
      }
   },
   {
      "id": 3,
      "name": "trackpad-2017",
      "description": "Bluetooth 5.0 connectivity",
      "net_price_in_cents": 18602,
      "gross": {
         "country": {
            "code": "UK",
            "name": "United Kingdom",
            "vat_rate": 0.2
         },
         "price_in_cents": 22322
      }
   }
]
```
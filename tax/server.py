import decimal
import sys
import time
import os
import grpc
import ecommerce_pb2
import ecommerce_pb2_grpc
from concurrent import futures

class Ecommerce(ecommerce_pb2_grpc.TaxServicer):
    def ApplyTax(self, request, content):
        country = request.country
        product = request.product
        price = decimal.Decimal(product.net_price_in_cents) / 100
        gross_value = ecommerce_pb2.Gross
        
        if len(country.code) > 0 and price > 0:
            withtax_price = price + (price * decimal.Decimal(country.vat_rate))
            withtax_price_in_cents = int(withtax_price * 100)
            gross_value = ecommerce_pb2.Gross(country=country, price_in_cents=withtax_price_in_cents)

        product_with_gross = ecommerce_pb2.Product(id=product.id,
        name=product.name,
        description=product.description,
        net_price_in_cents=product.net_price_in_cents,
        gross=gross_value)

        return ecommerce_pb2.TaxResponse(product=product_with_gross)
    
if __name__ == '__main__':
    port = sys.argv[1] if len(sys.argv) > 1 else 443
    host = '[::]:%s' % port
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
    keys_dir = os.path.abspath(os.path.join('.', os.pardir, 'keys'))
    with open('%s/private.key' % keys_dir, 'rb') as f:
        private_key = f.read()
    with open('%s/cert.pem' % keys_dir, 'rb') as f:
        certificate_chain = f.read()
    server_credentials = grpc.ssl_server_credentials(((private_key, certificate_chain),))
    server.add_secure_port(host, server_credentials)
    ecommerce_pb2_grpc.add_TaxServicer_to_server(Ecommerce(), server)
    try:
        server.start()
        print('Running Tax Service on %s' % host)
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        server.stop(0)